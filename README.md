# Woid for Android

Woid is a cross-platform Android IDE for both Android and Desktop.

Woid aims to be a fully fledged IDE with gradle support, auto completion.

## Compiling

You can just clone the repository and open it in Android Studio and clicking run.

Cloning the repository: `git clone https://gitlab.com/Woid/WoidAndroid.git`

## Contributing

Here you can see a guide on contributing to Woid.

### Commiting
Please do try to follow the commiting style if you want to contribute, it makes the repository much more clean and consistent.
- Do not squash commits
- Explain what the commit does or what did you do in the commit message with less than 50 characters
- Use the commit description if you can't fit what you did in the commit message
- Use one of these prefixes in your commit messages:
    - `fix` When you fixed a bug or maybe a flaw within the codebase
    - `feat` When you added something within the codebase (can be anything)
    - `tweak` When you do a little tweak in the codebase, like a tiny UI change
    - `chore` When you do fixed something but it doesn't affect the app in terms of functionality
    - `refactor` When you refactored the code, like cleaning up the code
    
## License
This project is licensed under the [GNU GPLv3 LICENSE], check the [LICENSE] file for details.

```
Woid Copyright (C) 2022 TheClashFruit
This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
This is free software, and you are welcome to redistribute it
under certain conditions; type `show c' for details.
```


<!-- links -->
[GNU GPLv3 LICENSE]: https://www.gnu.org/licenses/gpl-3.0.en.html
[LICENSE]: https://gitlab.com/Woid/WoidAndroid/-/blob/main/LICENSE